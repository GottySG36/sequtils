// Sets of utilities to parse Fastq/a files
package sequtils

import (
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"os"
    "regexp"
    "strings"
)


type Fastq struct {
    Fasta
    Quality     string
}

type Fsq []*Fastq

// Read start of file to determiner if file is gzip compressed or not.
// Returns an error if there is a problem with the open file
func GetFileType(inf *os.File) (string, error) {
	buff := make([]byte, 512)
	_, err := inf.Read(buff)
	if err != nil && err != io.EOF {
	    return "", err
	}
	inf.Seek(0, os.SEEK_SET)

	return http.DetectContentType(buff), nil
}

// Variation of LoadFastq function that will apply a function has the fastq file is read
// with at most m fastq sequences loaded then write to file. 
// This is needed is some cases where the file is too large to load in memory
// The lq
//func LoadFastqApply(file string, m int, f func() (Fsq error))

// Function to read in a fastq file, compressed or not and load it in a type Fsq ([]*Fastq).
// Return an error if the file can't be opened or if there is an error while reading
func LoadFastq(file string) (Fsq, error) {
    var f *os.File
    var err error
    if file == "-" {
        f = os.Stdin
    } else {
        f, err = os.Open(file)
    }
    defer f.Close()
    if err != nil {
        return nil, err
    }
    var r *bufio.Reader
    // Determines if we need to use gzip
	fileType, err := GetFileType(f)
    if err != nil {
        return nil, err
    }

	if fileType == "application/x-gzip" {
		// Parsing with gzip reader
		inf, err := gzip.NewReader(f)
		if err != nil {
			return nil, err
		}
		r = bufio.NewReader(inf)
	} else {
		// Parsing without gzip, uncompressed file
		r = bufio.NewReader(f)
	}

	var isQltReading bool
    first := true
    var bs strings.Builder
    var bq strings.Builder
    var seq = new(Fastq)
    seqs := NewFsq()

    for {
        lr, more, err := r.ReadLine()
        if err == io.EOF {
            seq.Sequence = bs.String()
            seq.Quality  = bq.String()
            if seq.Sequence != "" {
                seqs = append(seqs, seq)
            }
            break
        } else if len(lr) == 0 {
            continue
        } else if err != nil {
            return nil, err
        }
        if lr[0] == '+' {
            isQltReading = true
            continue
        } else if (lr[0] == '>' || lr[0] == '@') && !isQltReading {
            if !first{
                seq.Sequence = bs.String()
                seq.Quality  = bq.String()
                seqs = append(seqs, seq)
                bs.Reset()
                bq.Reset()
            }
            first = false
            seq = NewFastq(trailing.ReplaceAllLiteralString(string(lr), ""), "", "")
            isQltReading = false
        } else if !isQltReading {
            bs.Write(lr)
        } else {
            bq.Write(lr)
            if !more {
                isQltReading = false
            }
        }
    }
    return seqs, nil
}


// Simple routine to open and create a reader for a fastq file. 
// Determines if the file is gzipped or not 
func OpenFastq(file string) (*bufio.Reader, *os.File, error) {
    var f *os.File
    var err error
    if file == "-" {
        f = os.Stdin
    } else {
        f, err = os.Open(file)
    }
    if err != nil {
        return nil, nil, err
    }
    var r *bufio.Reader
    // Determines if we need to use gzip
	fileType, err := GetFileType(f)
    if err != nil {
        return nil, nil, err
    }

	if fileType == "application/x-gzip" {
		// Parsing with gzip reader
		inf, err := gzip.NewReader(f)
		if err != nil {
			return nil, nil, err
		}
		r = bufio.NewReader(inf)
	} else {
		// Parsing without gzip, uncompressed file
		r = bufio.NewReader(f)
    }
    return r, f, nil
}


// Function to read in a fastq file N sequences at a time, compressed or not and load it in a type Fsq ([]*Fastq).
// Return an error if the file can't be opened or if there is an error while reading
func LoadNFastq(n int, r *bufio.Reader) (Fsq, bool, error) {
    var count int
    var err error
	var isQltReading bool
    first := true
    var bs strings.Builder
    var bq strings.Builder
    var seq = new(Fastq)
    seqs := NewFsq()
    var eof bool

    for {
        lr, more, err := r.ReadLine()
        if err == io.EOF {
            seq.Sequence = bs.String()
            seq.Quality  = bq.String()
            if seq.Sequence != "" {
                seqs = append(seqs, seq)
                count++
            }
            eof = true
            break
        } else if err != nil {
            return nil, eof, err
        } else if len(lr) == 0 {
            continue
        }
        if lr[0] == '+' {
            isQltReading = true
            continue
        } else if (lr[0] == '>' || lr[0] == '@') && !isQltReading {
            if !first{
                seq.Sequence = bs.String()
                seq.Quality  = bq.String()
                seqs = append(seqs, seq)
                count++
                bs.Reset()
                bq.Reset()
                if count == n {
                    return seqs, eof, nil
                }
            }
            first = false
            seq = NewFastq(trailing.ReplaceAllLiteralString(string(lr), ""), "", "")
            isQltReading = false
        } else if !isQltReading {
            bs.Write(lr)
        } else {
            bq.Write(lr)
            if !more {
                isQltReading = false
            }
        }
    }
    return seqs, eof, err
}


// Convinience function to create a Fsq struct
func NewFsq() Fsq {
    f := make(Fsq, 0)
    return f
}

// function to create a fastq struct from three strings
// Take three arguments,
// 1. h : The header to use (will automatically add the starting '@');
// 2. s : The sequence itself, and;
// 3. q : The quality of the sequence
func NewFastq(h, s, q string) *Fastq {
    seq := new(Fastq)
    seq.Header = fmt.Sprintf("@%v", headerStart.ReplaceAllString(h, "")) // Ensures that there is only one '@' at the start of our sequence
    seq.Sequence = s
    seq.Quality  = q
    return seq
}

// Convinience method to add a fasta struct to the Fna
func (f *Fsq) AddFastq(seq *Fastq) {
    (*f) = append(*f, seq)
}


// Changes the header to `h`
func (f *Fastq) Rename(h string) {
    f.Header = fmt.Sprintf("@%v", headerStart.ReplaceAllString(h, "")) // Ensures that there is only one '@' at the start of our sequence
}

func (f *Fastq) GetQuality(phred64 bool) float64 {
    phred := 33.0
    if phred64 {
        phred = 64.0
    }
    var qltSum float64
    for _, char := range f.Quality {
        qltSum += float64(char) - phred
    }
    return qltSum / float64(len(f.Quality))
}


func (f *Fsq) GetAvgQuality(phred bool) float64 {
    var sumQlt float64
    for _, seq := range *f {
        sumQlt += seq.GetQuality(phred)
    }
    return sumQlt / float64(len(*f))
}

// Index the loaded fasta file using fasta headers
// Assumes that all headers are unique, raises an error if not
func (f *Fsq) Index() (SeqIndex, error) {
    index := make(SeqIndex, len(*f))
    for idx, seq := range *f {
        _, exists := index[seq.Header]
        if exists {
            return nil, fmt.Errorf("DuplicateFastqError : %v\n", seq.Header)
        }
        index[seq.Header] = idx
    }
    return index, nil
}

// Method to get a sequence from a Fna using a SeqIndex.
// Requires that the fasta be Indexed first
// Return the desired sequence and an error.
// Error should be NotFound, if any.
func (f *Fsq) Get(r string, fIdx SeqIndex) (*Fastq, error) {
    req := r
    if r[0] != '@' {
        req = fmt.Sprintf("@%v", r)
    }
    if idx, ok := fIdx[req]; !ok {
        return nil, fmt.Errorf("NotFound")
    } else {
        return (*f)[idx], nil
    }
}

// Simple Utility to filter the lengths of fasta sequences.
// Iterates over a slice of Sequences (Fna) and filters out the sequences
// whose lengths are lower or greater than the specified mininimum and maximum.
// If maximum <= 0, max is set to the length of the sequence.
// Returns filtered Fna
func (f *Fsq) FilterLength(min, max int) {
    var mx int
    n := 0
    for _, seq := range *f {
        if max <= 0 { mx = len(seq.Sequence) }
        if len(seq.Sequence) >= min && len(seq.Sequence) <= mx {
            (*f)[n] = seq
            n++
        }
    }
    *f = (*f)[:n]
}

// Utility to determine if an id+substr is found in a sequence header
func MatchFastqId(header, id, substr string) bool {
    var seek *regexp.Regexp
    if headerStart.MatchString(id) {
        seek = regexp.MustCompile(fmt.Sprintf("^%v%v", id, substr))
    } else {
        seek = regexp.MustCompile(fmt.Sprintf("^@%v%v", id, substr))
    }
    return seek.MatchString(header)
}

// Swap method to swap two items in an index fasta
func (f *Fsq) Swap(i, j int, sIdx SeqIndex) {
    // Swap the indexes
    sIdx[(*f)[i].Header], sIdx[(*f)[j].Header] = j, i
    // Swap the elements
    (*f)[i], (*f)[j] = (*f)[j], (*f)[i]
}

// Utility to filter out fastq sequences given a list of ids.
// Iterates over a slice of Sequences (Fna) and filters out the sequences 
// whose headers have been provided.
// There are two modes here. exact or substring match. If exact is true, we use the index to 
// search for the headers. The requires the provided headers to be an exact match to those 
// found in the fastq file. 
// If exact is set to false, Then we need to iterate over the index key and look for a partial 
// match in the headers. This may return multiple results per provided id.
// In the event that the headers aren't exact, it is possible to provide a pattern to limit the number 
// of results reported by the filter. for instance, if the header is composed of two space seperated 
// strings, you could provide `\s+` to the regexp parser using the `substr` argument. 
// It is also possible to reverse the result, meaning removing the provided sequences instead of 
// only keeping then. Set exclude to true if that is the desired output. 
// Also, by default, it will return an error any of the provided headers are not found. This 
// behavior can be modified with warn set to true, which will tell the user an id was not found instead
func (f *Fsq) FilterSequences(ids []string, idx SeqIndex, substr string, exact, exclude, warn bool) error {
    n := 0
    for _, seqId := range ids {
        if exact {
            fq, err := f.Get(seqId, idx)
            // If found swap
            if err != nil {
                if err.Error() == "NotFound" {
                    if warn {
                        fmt.Fprintf(os.Stderr, "Info -:- NotFound %q\n", seqId)
                        continue
                    }
                }
                return err
            }
            f.Swap(n, idx[fq.Header], idx)
            n++
        } else {
            var found bool
            for k, _ := range idx {
                if MatchFastqId(k, seqId, substr) {
                    found = true
                    fq, _ := f.Get(k, idx)
                    f.Swap(n, idx[fq.Header], idx)
                    n++
                    break
                }
            }
            if !found {
                if warn {
                    fmt.Fprintf(os.Stderr, "Info -:- NotFound %q\n", seqId)
                    continue
                } else {
                    return fmt.Errorf("NotFound : %q\n", seqId)
                }
            }
        }
    }
    if exclude {
        *f = (*f)[n:len(*f)]
    } else {
        *f = (*f)[:n]
    }

    return nil
}

func compare(s, to string, m int) bool {
    var nMism int
    if s == to {
        return true
    } else {
        for idx, _ := range s {
            if s[idx] != to[idx] {
                nMism++
                if nMism >= m {
                    return false
                }
                continue
            }
        }
    }
    return false
}

func (s *Fastq) containsIndex(i string, b, e, m int) (bool, error) {
    if len(i) > len(s.Sequence) {
        return false, fmt.Errorf("ERROR -:- Index length is greater than the sequence itself")
    } else if b > e {
        return false, fmt.Errorf("ERROR -:- Window start greater than end position")
    } else if e > len(s.Sequence) {
        return false, fmt.Errorf("ERROR -:- Window end position is greater than the sequence itself")
    } else if e - b < len(i) {
        return false, fmt.Errorf("ERROR -:- Window range smaller than index length")
    }

    e = e - len(i)
    r := e - b
    if r > len(s.Sequence) {
        // Should never happen because of check #3
        r = len(s.Sequence)
    }
    // First look for a perfect match
    for nb := b; nb <= e; nb++ {
        if i == s.Sequence[nb:nb+len(i)] {
            return true, nil
        }
    }
    // Look for a partial match with a tolerance of m mismatch
    if m > 0 {
        for nb := b; nb <= e; nb++ {
            if comp := compare(i, s.Sequence[nb:nb+len(i)], m); comp {
                return true, nil
            }
        }
    }
    return false, nil
}

func (f *Fsq) SearchIndex(i string, b, e, m int) error {
    n := 0
    for _, seq := range *f {
        comp, err := seq.containsIndex(i, b, e, m)
        if err != nil {
            return err
        }
        if comp {
            (*f)[n] = seq
            n++
        }
    }
    *f = (*f)[:n]
    return nil
}



func (f *Fastq) Subsequence(s, e int) (*Fastq, error) {
    subs := NewFastq(f.Header, "", "")

    if s > len((*f).Sequence) || s > len((*f).Quality){
        return f, fmt.Errorf("Start of subsequence (%v) exceeds sequence length (s:%v,q:%v).", s, len((*f).Sequence), len((*f).Quality))
    }
    if e >= len((*f).Sequence) || e >= len((*f).Quality) {
        return f, fmt.Errorf("End of subsequence (%v) exceeds sequence length (s:%v,q:%v).", e, len((*f).Sequence), len((*f).Quality))
    }

    (*subs).Sequence = (*f).Sequence[s:e+1]
    (*subs).Quality  = (*f).Quality[s:e+1]

    return subs, nil
}

func CreateFile(o string) (*bufio.Writer, *os.File, error) {
    var of *os.File
    var err error
    if o == "-" {
        of = os.Stdout
    } else {
        of, err = os.Create(o)
    }
    if err != nil {
        return nil, nil, err
    }

    w := bufio.NewWriter(of)

    return w, of, err
}

func (f Fsq) WriteAppend(w *bufio.Writer) error {
    var err error
    for _, l := range f {
        if _, err = w.WriteString(l.Header); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if _, err = w.WriteString(l.Sequence); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if err = w.WriteByte('+'); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if _, err = w.WriteString(l.Quality); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
    }
    w.Flush()
    return nil
}

func (f Fsq) Write(o string) error {
    var of *os.File
    var err error
    if o == "-" {
        of = os.Stdout
    } else {
        of, err = os.Create(o)
    }
    defer of.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(of)

    for _, l := range f {
        if _, err = w.WriteString(l.Header); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if _, err = w.WriteString(l.Sequence); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if err = w.WriteByte('+'); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if _, err = w.WriteString(l.Quality); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
    }
    w.Flush()
    return nil
}

func CreateFileGzip(o string) (*gzip.Writer, *os.File, error) {
    var of *os.File
    var err error
    if o == "-" {
        of = os.Stdout
    } else {
        of, err = os.Create(o)
    }
    if err != nil {
        return nil, nil, err
    }

    w := gzip.NewWriter(of)

    return w, of, err
}

func (f Fsq) WriteAppendGzip(w *gzip.Writer) error {
    var err error
    for _, l := range f {
        if _, err = w.Write([]byte(l.Header)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte(l.Sequence)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte("+")); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte(l.Quality)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
    }
    w.Flush()
    return nil
}

func (f Fsq) WriteGzip(o string) error {
    var of *os.File
    var err error
    if o == "-" {
        of = os.Stdout
    } else {
        of, err = os.Create(o)
    }
    defer of.Close()
    if err != nil {
        return err
    }

    w := gzip.NewWriter(of)

    for _, l := range f {
        if _, err = w.Write([]byte(l.Header)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte(l.Sequence)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte("+")); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
        if _, err = w.Write([]byte(l.Quality)); err != nil {
            return err
        }
        if _, err = w.Write([]byte("\n")); err != nil {
            return err
        }
    }
    w.Flush()
    w.Close()
    return nil
}
