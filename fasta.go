// Sets of utilities to parse Fastq/a files
package sequtils

import (
	"bufio"
	"fmt"
	"io"
	"os"
    "regexp"
    "strings"
)

var (
    headerStart = regexp.MustCompile(`^[>@]+`)
    trailing    = regexp.MustCompile(`(^\s+)|(\s+$)`)
)

type Fasta struct {
    Header      string
    Sequence    string
}

type Fna []*Fasta

func LoadFasta(file string) (Fna, error) {
    var f *os.File
    var err error
    if file == "-" {
        f = os.Stdin
    } else {
        f, err = os.Open(file)
    }
    defer f.Close()
    if err != nil {
        return nil, err
    }

    first := true
    var b strings.Builder
    var seq = new(Fasta)
    seqs := make(Fna, 0)

    r := bufio.NewReader(f)
    for {
        lr, _, err := r.ReadLine()
        if err == io.EOF {
            seq.Sequence = b.String()
            if seq.Sequence != "" {
                seqs = append(seqs, seq)
            }
            break
        } else if len(lr) == 0 {
            continue
        } else if err != nil {
            return nil, err
        }
        if lr[0] == '>' || lr[0] == '@' {
            if !first {
                seq.Sequence = b.String()
                seqs = append(seqs, seq)
                b.Reset()
            }
            first = false
            seq = &Fasta{Header:trailing.ReplaceAllLiteralString(string(lr), "")}
        } else {
            b.Write(lr)
        }
    }
    return seqs, nil
}

// Convinience function to create a Fna struct
func NewFna() Fna {
    f := make(Fna, 0)
    return f
}

// function to create a fasta struct from two strings
// Take two arguments, 
// 1. h : The header to use (will automatically add the starting '>')
// 2. s : The sequence itself. 
func NewFasta(h, s string) *Fasta {
    seq := new(Fasta)
    seq.Header = fmt.Sprintf(">%v", headerStart.ReplaceAllString(h, "")) // Ensures that there is only one '>' at the start of our sequence 
    seq.Sequence = s
    return seq
}

// Convinience method to add a fasta struct to the Fna
func (f *Fna) AddFasta(seq *Fasta) {
    (*f) = append(*f, seq)
}

// Changes the header to `h`
func (f *Fasta) Rename(h string) {
    f.Header = fmt.Sprintf(">%v", headerStart.ReplaceAllString(h, "")) // Ensures that there is only one '>' at the start of our sequence
}

// Type used to keep track of old and new header names.
// For use with Replace, AddHeaderPrefix and AddHeaderSuffix
type Association struct {
    Old string
    New string
}

type Converter []*Association

// New type that will replace Converter TODO
// map[old]new
type MapConverter map[string]string

// Convenience method to create a new converter and allocate enough space for `nb` Associations
func NewConverter(nb int) Converter  {
    c := make(Converter, nb)
    return c
}

func NewMapConverter() MapConverter  {
    c := make(MapConverter)
    return c
}

func (c MapConverter) AddAssociation(o, n string) error {
    if _, exists := c[o]; exists {
        return fmt.Errorf("DuplicateHeaderError")
    }
    c[o] = n
    return nil
}

// Adds an `Association` to the converter, replacing any existing one at position `idx`
// Return an error if `idx >= len(c)`
func (c Converter) AddAssociation(idx int, o, n string) error {
    if idx >= len(c) {
        return fmt.Errorf("IndexOutOfRange : received %v, but have %v elements.", idx, len(c))
    }
    c[idx] = &Association{Old:o, New:n}
    return nil
}

// Replaces all existing headers by `h` follow by a sequential number
// Return the associated Converter and an error.
func (f *Fna)  ReplaceHeader(h string) (Converter, error) {
    c := NewConverter(len(*f))
    for idx, seq := range *f {
        n := fmt.Sprintf(">%v_%v", h, idx)

        // Save old/new sequence header
        err := c.AddAssociation(idx, seq.Header, n)
        if err != nil {
            return c, err
        }
        seq.Rename(n)
    }
    return c, nil
}

// Adds prefix `h` to the sequences' header seperating both by delimiter `d`
// Return the associated Converter and an error.
func (f *Fna) AddHeaderPrefix(h, d string) (Converter, error) {
    c := NewConverter(len(*f))
    for idx, seq := range *f {
        n := fmt.Sprintf(">%v%v%v", h, d, (*seq).Header[1:])

        // Save old/new sequence header
        err := c.AddAssociation(idx, (*seq).Header, n)
        if err != nil {
            return c, err
        }
        seq.Rename(n)
    }
    return c, nil
}

// Adds prefix `h` to the sequences' header seperating both by delimiter `d`
// Return the associated Converter and an error.
func (f *Fna) AddHeaderSuffix(h, d string) (Converter, error) {
    c := NewConverter(len(*f))
    for idx, seq := range *f {
        n := fmt.Sprintf(">%v%v%v", (*seq).Header[1:], d, h)

        // Save old/new sequence header
        err := c.AddAssociation(idx, (*seq).Header, n)
        if err != nil {
            return c, err
        }
        seq.Rename(n)
    }
    return c, nil
}

// TODO
func (f *Fna) HeaderFromConverter(c MapConverter) error {
    for _, seq := range *f {
        n, exists := c[seq.Header]
        if !exists {
            return fmt.Errorf("HeaderNotInConverter : %v", seq.Header)
        }
        seq.Rename(n)
    }
    return nil
}

func LoadConverter(f string, flip bool) (MapConverter, error) {
    of, err := os.Open(f)
    defer of.Close()
    if err != nil {
        return nil, err
    }
    c := NewMapConverter()
    r := bufio.NewScanner(of)
    for r.Scan() {
        names := strings.Split(r.Text(), ",")
        if !headerStart.MatchString(names[0]) {
            names[0] = fmt.Sprintf(">%v", names[0])
        }
        if !headerStart.MatchString(names[1]) {
            names[1] = fmt.Sprintf(">%v", names[1])
        }

        if flip {
            err = c.AddAssociation(names[1], names[0])
        } else {
            err = c.AddAssociation(names[0], names[1])
        }
        if err != nil {
            return nil, err
        }
    }
    return c, nil
}

func (c Converter) ToCsv(o string) error {
    f, err := os.Create(o)
    defer f.Close()
    if err != nil {
        return err
    }
    w := bufio.NewWriter(f)

    for _, l := range c {
        if _, err = w.WriteString((*l).Old); err != nil {
            return err
        }
        if err = w.WriteByte(','); err != nil {
            return err
        }
        if _, err = w.WriteString((*l).New); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
    }
    w.Flush()
    return nil
}

type SeqIndex map[string]int

// Index the loaded fasta file using fasta headers
// Assumes that all headers are unique, raises an error if not
func (f *Fna) Index() (SeqIndex, error) {
    index := make(SeqIndex, len(*f))
    for idx, seq := range *f {
        _, exists := index[seq.Header]
        if exists {
            return nil, fmt.Errorf("DuplicateFastaError : %v\n", seq.Header)
        }
        index[seq.Header] = idx
    }
    return index, nil
}

// Method to get a sequence from a Fna using a SeqIndex.
// Requires that the fasta be Indexed first
// Return the desired sequence and an error.
// Error should be NotFound, if any.
func (f *Fna) Get(r string, fIdx SeqIndex) (*Fasta, error) {
    req := r
    if r[0] != '>' {
        req = fmt.Sprintf(">%v", r)
    }
    if idx, ok := fIdx[req]; !ok {
        return nil, fmt.Errorf("NotFound")
    } else {
        return (*f)[idx], nil
    }
}

// Simple Utility to filter the lengths of fasta sequences.
// Iterates over a slice of Sequences (Fna) and filters out the sequences 
// whose lengths are lower or greater than the specified mininimum and maximum.
// If maximum <= 0, max is set to the length of the sequence. 
// Returns filtered Fna
func (f *Fna) FilterLength(min, max int) {
    var mx int
    n := 0
    for _, seq := range *f {
        if max <= 0 { mx = len(seq.Sequence) }
        if len(seq.Sequence) >= min && len(seq.Sequence) <= mx {
            (*f)[n] = seq
            n++
        }
    }
    *f = (*f)[:n]
}

// Utility to determine if an id+substr is found in a sequence header
func MatchFastaId(header, id, substr string) bool {
    var seek *regexp.Regexp
    if headerStart.MatchString(id) {
        seek = regexp.MustCompile(fmt.Sprintf("^%v%v", id, substr))
    } else {
        seek = regexp.MustCompile(fmt.Sprintf("^>%v%v", id, substr))
    }
    return seek.MatchString(header)
}

// Swap method to swap two items in an index fasta
func (f *Fna) Swap(i, j int, sIdx SeqIndex) {
    // Swap the indexes
    sIdx[(*f)[i].Header], sIdx[(*f)[j].Header] = j, i
    // Swap the elements
    (*f)[i], (*f)[j] = (*f)[j], (*f)[i]
}

// Utility to filter out fasta sequences given a list of ids.
// Iterates over a slice of Sequences (Fna) and filters out the sequences 
// whose headers have been provided.
// There are two modes here. exact or substring match. If exact is true, we use the index to 
// search for the headers. The requires the provided headers to be an exact match to those 
// found in the fasta file. 
// If exact is set to false, Then we need to iterate over the index key and look for a partial 
// match in the headers. This may return multiple results per provided id.
// In the event that the headers aren't exact, it is possible to provide a pattern to limit the number 
// of results reported by the filter. for instance, if the header is composed of two space seperated 
// strings, you could provide `\s+` to the regexp parser using the `substr` argument. 
// It is also possible to reverse the result, meaning removing the provided sequences instead of 
// only keeping then. Set exclude to true if that is the desired output. 
// Also, by default, it will return an error any of the provided headers are not found. This 
// behavior can be modified with warn set to true, which will tell the user an id was not found instead
func (f *Fna) FilterSequences(ids []string, idx SeqIndex, substr string, exact, exclude, warn bool) error {
    n := 0
    for _, seqId := range ids {
        if exact {
            fas, err := f.Get(seqId, idx)
            // If found swap
            if err != nil {
                if err.Error() == "NotFound" {
                    if warn {
                        fmt.Fprintf(os.Stderr, "Info -:- NotFound %q\n", seqId)
                        continue
                    }
                }
                return err
            }
            f.Swap(n, idx[fas.Header], idx)
            n++
        } else {
            var found bool
            for k, _ := range idx {
                if MatchFastaId(k, seqId, substr) {
                    found = true
                    fas, _ := f.Get(k, idx)
                    f.Swap(n, idx[fas.Header], idx)
                    n++
                    break
                }
            }
            if !found {
                if warn {
                    fmt.Fprintf(os.Stderr, "Info -:- NotFound %q\n", seqId)
                    continue
                } else {
                    return fmt.Errorf("NotFound : %q\n", seqId)
                }
            }
        }
    }
    if exclude {
        *f = (*f)[n:len(*f)]
    } else {
        *f = (*f)[:n]
    }

    return nil
}

func (f *Fasta) Subsequence(s, e int) (*Fasta, error) {
    subs := Fasta{Header:f.Header}

    if s > len((*f).Sequence) {
        return f, fmt.Errorf("Start of subsequence (%v) exceeds sequence length (%v).", s, len((*f).Sequence))
    }
    if e >= len((*f).Sequence) {
        return f, fmt.Errorf("End of subsequence (%v) exceeds sequence length (%v).", e, len((*f).Sequence))
    }

    subs.Sequence = (*f).Sequence[s:e+1]

    return &subs, nil
}

func (f Fna) Write(o string) error {
    var of *os.File
    var err error
    if o == "-" {
        of = os.Stdout
    } else {
        of, err = os.Create(o)
    }
    defer of.Close()
    if err != nil {
        return err
    }
    w := bufio.NewWriter(of)

    for _, l := range f {
        if _, err = w.WriteString(l.Header); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
        if _, err = w.WriteString(l.Sequence); err != nil {
            return err
        }
        if err = w.WriteByte('\n'); err != nil {
            return err
        }
    }
    w.Flush()
    return nil
}

var re = regexp.MustCompile(`\s`)

// Could be merged / removed because it can be accomplished with 
// combination of simpler function/methods combination 
// Load, Index, NewFasta, AddSequence, Write
func ExtractFna(file, prefix, taxid string, heads map[string]bool) error {
    f, err := os.Open(file)
    defer f.Close()
    if err != nil {
        return err
    }
    r := bufio.NewReader(f)

    out := fmt.Sprintf("%v/genome_taxid_%v.fasta", prefix, taxid)

    o, err := os.OpenFile(out, os.O_CREATE|os.O_WRONLY, 0644)
    defer o.Close()
    if err != nil {
        return err
    }
    w := bufio.NewWriter(o)
    var write bool
    var counter int
    newline := ""
    for {
        lr, _, err := r.ReadLine()
        line := fmt.Sprintf("%s", lr)
        if err == io.EOF {
            break
        } else if len(line) == 0 {
            continue
        }

        if line[0] == '>' {
            write = false
            header := re.ReplaceAllLiteralString(line[1:], "")
            if heads[header] {
                counter++
                write = true
                w.WriteString(newline)
                w.WriteString(line)
                w.WriteString("\n")
                newline = "\n"
            }
        } else if write {
            w.WriteString(line)
        }
    }

    w.Flush()
    if counter != len(heads) {
        fmt.Printf("WARNING -:- Found %v of %v for taxid %v\n", counter, len(heads), taxid)
    }
    return nil
}

